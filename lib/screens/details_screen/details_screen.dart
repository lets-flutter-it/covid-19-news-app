import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:news_app/models/news.dart';
import 'package:news_app/screens/details_screen/news_details_text.dart';

class DetailsScreen extends StatelessWidget {
  const DetailsScreen({this.news});
  final News news;

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(0xFF121234),
        leading: IconButton(
          icon: Icon(Icons.chevron_left),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      backgroundColor: Color(0xFF121234),
      body: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.vertical(top: Radius.circular(30)),
        ),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 4),
            child: Text(
              news.title,
              style: GoogleFonts.vidaloka(
                  color: Color(0xFF121234),
                  fontSize: 25,
                  fontWeight: FontWeight.bold),
              softWrap: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 30, right: 30, bottom: 10),
            child: Text(
              '12 Jun 2020',
              style: TextStyle(
                color: Color(0xFF121234),
                fontSize: 10,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          Expanded(
            child: Stack(
              children: [
                Positioned(
                  child: Image.asset(
                    news.urlToImage,
                    width: MediaQuery.of(context).size.width,
                    fit: BoxFit.cover,
                    height: height * .3,
                  ),
                  top: 0,
                ),
                Positioned.fill(
                  top: height * .3 - 25,
                  child: NewsDetailsText(news.description),
                ),
              ],
            ),
          ),
        ]),
      ),
    );
  }
}
