import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:news_app/blocs/auth/auth_bloc.dart';
import 'package:news_app/blocs/auth/auth_event.dart';
import 'package:news_app/blocs/auth/auth_state.dart';
import 'package:news_app/blocs/news/news_bloc.dart';
import 'package:news_app/blocs/news/news_state.dart';
import 'package:news_app/models/user.dart';
import 'package:news_app/screens/bookmark_screen/bookmark_screen.dart';

import 'package:news_app/screens/signin_screen/signin_screen.dart';
import 'package:news_app/screens/signup_screen/signup_screen.dart';
import 'package:news_app/widgets/news_card.dart';

class HomeScreen extends StatelessWidget {
  HomeScreen({this.user});
  final User user;
  @override
  Widget build(BuildContext context) {
    final authBloc = BlocProvider.of<AuthBloc>(context);
    return BlocBuilder<AuthBloc, AuthState>(
      builder: (context, state) => Scaffold(
        drawer: Drawer(
            child: state is Authenticated
                ? ListView(
                    padding: EdgeInsets.zero,
                    children: [
                      DrawerHeader(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                        ),
                        child: Center(
                          child: FlutterLogo(size: 50,),
                        ),
                      ),
                      ListTile(
                        leading: Icon(Icons.bookmark),
                        title: Text('Bookmarks',
                        style: TextStyle(fontSize: 18),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(MaterialPageRoute(
                            builder: (context) => BookmarkScreen(),
                          ));
                        },
                      ),
                    ],
                  )
                : ListView(
                    padding: EdgeInsets.zero,
                    children: [
                      DrawerHeader(
                        decoration: BoxDecoration(
                          color: Theme.of(context).primaryColor,
                        ),
                        child: Center(
                          child: FlutterLogo(
                            size: 50,
                          ),
                        ),
                      ),
                      ListTile(
                        leading: FaIcon(FontAwesomeIcons.signInAlt),
                        title: Text(
                          'Sign up',
                          style: TextStyle(fontSize: 18),
                        ),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => SignUpScreen(),
                            ),
                          );
                        },
                      ),
                      ListTile(
                        leading: FaIcon(FontAwesomeIcons.signInAlt),
                        title: Text('Sign in',
                        style: TextStyle(fontSize: 18),),
                        onTap: () {
                          Navigator.pop(context);
                          Navigator.of(context).push(
                            MaterialPageRoute(
                              builder: (_) => SignInScreen(),
                            ),
                          );
                        },
                      ),
                    ],
                  )),
        backgroundColor: Colors.white,
        appBar: AppBar(
          title: Text(
            'Corona News',
            style: GoogleFonts.playfairDisplay(
                fontWeight: FontWeight.bold, fontSize: 23),
          ),
          actions: [
            state is Authenticated
                ? IconButton(
                    icon: Icon(Icons.exit_to_app),
                    iconSize: 28,
                    onPressed: () => BlocProvider.of<AuthBloc>(context)
                        .add(Unauthenticate()),
                  )
                : IconButton(
                    icon: Icon(Icons.add_to_home_screen),
                    onPressed: () =>
                        Navigator.of(context).push(MaterialPageRoute(
                      builder: (context) => SignInScreen(),
                    )),
                  ),
          ],
          centerTitle: true,
          backgroundColor: Color(0xFF121234),
          elevation: 0.0,
        ),
        body: BlocBuilder<NewsBloc, NewsState>(
          builder: (context, state) => state is LoadingNews
              ? Center(
                  child: CircularProgressIndicator(
                  backgroundColor: Color(0xFF121234),
                ))
              : LayoutBuilder(
                  builder: (
                    BuildContext context,
                    BoxConstraints viewportConstraints,
                  ) =>
                      SingleChildScrollView(
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: viewportConstraints.maxHeight,
                      ),
                      child: Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            ...state.feed.map(
                              (feed) => NewsCard(
                                news: feed,
                                user: user,
                                isAuth: authBloc.state is Authenticated,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
        ),
      ),
    );
  }
}
