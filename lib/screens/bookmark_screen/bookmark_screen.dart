import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:news_app/blocs/auth/auth_bloc.dart';
import 'package:news_app/blocs/auth/auth_event.dart';
import 'package:news_app/blocs/news/news_bloc.dart';
import 'package:news_app/blocs/news/news_event.dart';
import 'package:news_app/blocs/news/news_state.dart';
import 'package:news_app/models/user.dart';
import 'package:news_app/screens/news_screen/home_screen.dart';
import 'package:news_app/widgets/news_card.dart';

class BookmarkScreen extends StatelessWidget {
  const BookmarkScreen({this.user});
  final User user;

  @override
  Widget build(BuildContext context) {
    BlocProvider.of<NewsBloc>(context).add(FetchBookmarks());
    return Scaffold(
      drawer: Drawer(
          child: ListView(
        padding: EdgeInsets.zero,
        children: [
          DrawerHeader(
            decoration: BoxDecoration(
              color: Theme.of(context).primaryColor,
            ),
            child: Center(child: FlutterLogo(size: 50,)),
          ),
          ListTile(
            leading: Icon(Icons.home),
            title: Text('Home',style: TextStyle(fontSize: 18),),
            onTap: () {
              Navigator.pop(context);
              Navigator.of(context).push(MaterialPageRoute(
                builder: (context) => HomeScreen(),
              ));
            },
          ),
        ],
      )),
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text(
          'Corona News',
          style: GoogleFonts.playfairDisplay(
              fontWeight: FontWeight.bold, fontSize: 23),
        ),
        actions: [
          IconButton(
            icon: Icon(Icons.exit_to_app),
            iconSize: 28,
            onPressed: () =>
                BlocProvider.of<AuthBloc>(context).add(Unauthenticate()),
          ),
        ],
        centerTitle: true,
        backgroundColor: Color(0xFF121234),
        elevation: 0.0,
      ),
      body: BlocBuilder<NewsBloc, NewsState>(
        builder: (_, state) => state is LoadingNews
            ? Center(
                child: CircularProgressIndicator(
                backgroundColor: Color(0xFF121234),
              ))
            : LayoutBuilder(
                builder: (
                  BuildContext context,
                  BoxConstraints viewportConstraints,
                ) =>
                    SingleChildScrollView(
                  child: ConstrainedBox(
                    constraints: BoxConstraints(
                      minHeight: viewportConstraints.maxHeight,
                    ),
                    child: state.bookmarked.isEmpty
                        ? Text('No bookmarks')
                        : Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                ...state.bookmarked.map(
                                  (bookmark) => NewsCard(
                                    news: bookmark,
                                    user: user,
                                    isAuth: true,
                                  ),
                                ),
                              ],
                            ),
                          ),
                  ),
                ),
              ),
      ),
    );
  }

  //TODO: WebFeed()
  //TODO: chached image package

}
