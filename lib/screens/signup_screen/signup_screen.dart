import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:news_app/blocs/auth/auth_bloc.dart';
import 'package:news_app/blocs/signup/signup_bloc.dart';
import 'package:news_app/blocs/signup/signup_event.dart';
import 'package:news_app/blocs/signup/signup_state.dart';
import 'package:news_app/util/snackbar.dart';
import 'package:news_app/widgets/sign_button.dart';
import 'package:news_app/widgets/sign_field.dart';
import 'package:news_app/widgets/sign_screen.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignUpScreen extends StatelessWidget {
  const SignUpScreen();

  @override
  Widget build(BuildContext context) {
    final signupBloc = SignUpBloc(BlocProvider.of<AuthBloc>(context));
    return BlocProvider(
      create: (_) => signupBloc,
      child: SignScreen(
        title: 'Register',
        body: BlocConsumer<SignUpBloc, SignUpState>(
          listener: (context, state) {
            if (state is SignUpError) Snackbar.showError(context, state.error);
          },
          buildWhen: (previous, current) => current is! SignUpError,
          listenWhen: (previous, current) => current is SignUpError,
          builder: (context, state) {
            return SingleChildScrollView(
              child: SafeArea(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Navigator.of(context).canPop()
                        ? IconButton(
                            icon: Icon(Icons.arrow_back),
                            color: Theme.of(context).primaryColor,
                            onPressed: () => Navigator.of(context).pop())
                        : SizedBox(),
                    // Center(
                    //     child: FlutterLogo(
                    //   size: 45,
                    //   colors: Colors.indigo,
                    // )),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 40, vertical: 30),
                      child: Center(
                        child: Text(
                          'Welcome to COVID-19 News App',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.megrim(
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 25, vertical: 14),
                      child: Text(
                        'Create your Account',
                        style: GoogleFonts.poppins(
                          fontWeight: FontWeight.bold,
                          fontSize: 20,
                          color: Theme.of(context).primaryColor,
                        ),
                      ),
                    ),
                    SignField(
                      controller: signupBloc.nameController,
                      hint: 'Name',
                    ),
                    SignField(
                      controller: signupBloc.emailController,
                      hint: 'Email',
                    ),
                    SignField(
                      controller: signupBloc.passwordController,
                      hint: 'Password',
                      obscure: true,
                    ),
                    if (state is Loading)
                      CircularProgressIndicator()
                    else
                      Padding(
                        padding: const EdgeInsets.all(10.0),
                        child: Center(
                          child: SignButton(
                            'Sign up',
                            onPressed: () => signupBloc.add(SignUp()),
                          ),
                        ),
                      ),

                    Padding(
                      padding: const EdgeInsets.only(top: 20),
                      child: Center(
                        child: Text(
                          '..Or sign up with..',
                          style: GoogleFonts.poppins(
                            fontSize: 15,
                            color: Theme.of(context).primaryColor,
                          ),
                        ),
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(bottom: 15),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: [
                          CompanyIcon(
                            icon: FontAwesomeIcons.google,
                            onPressed: () {},
                          ),
                          CompanyIcon(
                            icon: FontAwesomeIcons.facebookF,
                            onPressed: () {},
                          ),
                          CompanyIcon(
                            icon: FontAwesomeIcons.twitter,
                            onPressed: () {},
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class CompanyIcon extends StatelessWidget {
  const CompanyIcon({this.icon, this.onPressed});
  final IconData icon;
  final Function onPressed;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: IconButton(
        icon: Icon(
          icon,
        ),
        onPressed: onPressed,
      ),
    );
  }
}
