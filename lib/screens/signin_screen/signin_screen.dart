import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_app/blocs/auth/auth_bloc.dart';
import 'package:news_app/blocs/signin/signin_bloc.dart';
import 'package:news_app/blocs/signin/signin_state.dart';
import 'package:news_app/screens/signin_screen/signin_form.dart';
import 'package:news_app/util/snackbar.dart';

import 'package:news_app/widgets/sign_screen.dart';

class SignInScreen extends StatelessWidget {
  const SignInScreen();

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => SignInBloc(BlocProvider.of<AuthBloc>(context)),
        child: SignScreen(
          title: 'Login',
          body: BlocListener<SignInBloc, SignInState>(
            listener: (_, state) {
              if (state is SignInError) {
                Snackbar.showError(context, state.error);
              }
            },
            child: SignInForm(),
          ),
        ),
      );
}
