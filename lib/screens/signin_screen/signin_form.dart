import 'package:flushbar/flushbar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:news_app/blocs/signin/signin_bloc.dart';
import 'package:news_app/blocs/signin/signin_state.dart';
import 'package:news_app/blocs/signin/signin_event.dart';
import 'package:news_app/screens/signup_screen/signup_screen.dart';
import 'package:news_app/widgets/sign_button.dart';
import 'package:news_app/widgets/sign_field.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SignInForm extends StatelessWidget {
  const SignInForm();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<SignInBloc, SignInState>(
      builder: (context, state) {
        final signinBloc = BlocProvider.of<SignInBloc>(context);

        return SafeArea(
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Navigator.of(context).canPop()
                    ? IconButton(
                        icon: Icon(Icons.arrow_back),
                        color: Theme.of(context).primaryColor,
                        onPressed: () => Navigator.of(context).pop())
                    : SizedBox(),
                // Center(
                //     child: FlutterLogo(
                //   size: 45,
                //   colors: Colors.indigo,
                // )),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 40, vertical: 30),
                  child: Center(
                    child: Text(
                      'Welcome to COVID-19 News App',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.megrim(
                        fontWeight: FontWeight.bold,
                        fontSize: 40,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 25, vertical: 14),
                  child: Text(
                    'Login to your Account',
                    style: GoogleFonts.poppins(
                      fontWeight: FontWeight.bold,
                      fontSize: 20,
                      color: Theme.of(context).primaryColor,
                    ),
                  ),
                ),
                SignField(
                  controller: signinBloc.emailController,
                  hint: 'Email',
                ),
                SignField(
                  controller: signinBloc.passwordController,
                  hint: 'Password',
                  obscure: true,
                ),
                if (state is Loading)
                  Center(child: CircularProgressIndicator(),)
                else
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Center(
                      child: SignButton(
                        'Sign in',
                        onPressed: () =>
                            signinBloc.add(SignInEvent.signInWithEmail()),
                      ),
                    ),
                  ),
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Center(
                    child: Text(
                      '..Or sign in with..',
                      style: GoogleFonts.poppins(
                        fontSize: 15,
                        color: Theme.of(context).primaryColor,
                      ),
                    ),
                  ),
                ),

                Padding(
                  padding: const EdgeInsets.only(bottom: 15),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      CompanyIcon(
                        icon: FontAwesomeIcons.google,
                        onPressed: () {},
                      ),
                      CompanyIcon(
                        icon: FontAwesomeIcons.facebookF,
                        onPressed: () {},
                      ),
                      CompanyIcon(
                        icon: FontAwesomeIcons.twitter,
                        onPressed: () {},
                      ),
                    ],
                  ),
                ),
                if (state is! Loading)
                  Center(
                    child: RichText(
                      text: TextSpan(
                        children: [
                          TextSpan(
                            text: "Don't have an account?",
                          ),
                          TextSpan(
                            text: 'Sign up',
                            recognizer: TapGestureRecognizer()
                              ..onTap = () => Navigator.of(context).push(
                                    MaterialPageRoute(
                                      builder: (context) => SignUpScreen(),
                                    ),
                                  ),
                          )
                        ],
                      ),
                    ),
                  ),
              ],
            ),
          ),
        );
      },
    );
  }
}
// Text("Don't have an account? Sign up"),
