class News {
  // add user name and email here?
  const News({
    this.author,
    this.title,
    this.description,
    this.url,
    this.urlToImage,
    this.publishedAt,
    this.content,
    this.isBookmarked = false,
  });

  final String author,
      title,
      description,
      url,
      urlToImage,
      publishedAt,
      content;

  final bool isBookmarked;

  factory News.fromJson(Map<String, Object> json) => News(
        author: json['author'],
        title: json["title"],
        description: json["description"],
        url: json["url"],
        urlToImage: json["urlToImage"],
        publishedAt: json["publishedAt"],
        content: json["content"],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'title': title,
        'description': description,
        'urlToImage': urlToImage,
        'publishedAt': publishedAt,
        'content': content,
        'url': url,
        'author': author,
      };
}
