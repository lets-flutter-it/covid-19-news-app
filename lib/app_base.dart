import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:news_app/blocs/auth/auth_event.dart';
import 'package:news_app/screens/news_screen/home_screen.dart';
import 'package:news_app/screens/signup_screen/splash_screen.dart';
import 'blocs/auth/auth_bloc.dart';
import 'blocs/auth/auth_state.dart';
import 'blocs/news/news_bloc.dart';
import 'blocs/news/news_event.dart';

class AppBase extends StatelessWidget {
  const AppBase();

  @override
  Widget build(BuildContext context) => BlocProvider(
        create: (_) => AuthBloc()..add(StartApp()),
        child: BlocProvider(
          create: (context) =>
              NewsBloc(BlocProvider.of<AuthBloc>(context))..add(FetchNews()),
          child: MaterialApp(
            debugShowCheckedModeBanner: false,
            theme: ThemeData(
              primaryColor: Color(0xFF121234),
            ),
            home: BlocBuilder<AuthBloc, AuthState>(
              builder: (context, state) {
                WidgetsBinding.instance.addPostFrameCallback((_) =>
                    Navigator.of(context).popUntil((route) => route.isFirst));
                return state.maybeWhen(
                  loadingApp: () => SplashScreen(),
                  orElse: () => HomeScreen(),
                );
              },
            ),
          ),
        ),
      );
}
