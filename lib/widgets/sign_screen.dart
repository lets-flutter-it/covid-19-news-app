import 'package:flutter/material.dart';

class SignScreen extends StatelessWidget {
  const SignScreen({this.title, this.body});
  final String title;
  final Widget body;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: body,
    );
  }
}
