import 'package:flutter/material.dart';

class SignButton extends StatelessWidget {
  const SignButton(this.buttonText, {@required this.onPressed});
  final String buttonText;
  final VoidCallback onPressed;
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      color: Theme.of(context).primaryColor,
      textColor: Colors.white,
      elevation: 4,
      padding: EdgeInsets.symmetric(horizontal: 110, vertical: 14),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: Text(buttonText),
    );
  }
}
