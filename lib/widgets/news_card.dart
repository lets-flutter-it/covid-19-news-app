import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:google_fonts/google_fonts.dart';

import 'package:news_app/blocs/news/news_bloc.dart';
import 'package:news_app/blocs/news/news_event.dart';
import 'package:news_app/models/news.dart';
import 'package:news_app/models/user.dart';
import 'package:webview_flutter/webview_flutter.dart';

class NewsCard extends StatelessWidget {
  const NewsCard({this.news, this.user, this.isAuth});

  final News news;
  final User user;
  final bool isAuth;

  @override
  Widget build(BuildContext context) => InkWell(
        onTap: () => Navigator.of(context).push(
          MaterialPageRoute(
            builder: (_) => Scaffold(
              appBar: AppBar(
                title: Text(
                  'Corona News',
                  style: GoogleFonts.playfairDisplay(
                      fontWeight: FontWeight.bold, fontSize: 23),
                ),
                centerTitle: true,
                backgroundColor: Color(0xFF121234),
                elevation: 0.0,
              ),
              body: WebView(
                initialUrl: news.url,
                javascriptMode: JavascriptMode.unrestricted,
              ),
            ),
          ),
        ),
        child: Card(
          elevation: 10,
          margin: EdgeInsets.all(12),
          color: Colors.white, //Color(0xFF121234), //Color(0xFFc70039),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.fromLTRB(13.0, 13.0, 13.0, 0),
                child: Row(
                  children: [
                    CircleAvatar(
                        backgroundColor: Theme.of(context).primaryColor,
                        child: news.author != null
                            ? Text(
                                news.author.substring(0,2),
                                style: TextStyle(color: Colors.white),
                              )
                            : Icon(Icons.person)),
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      news.author == null ? 'Unknown' : news.author.length > 18 ? news.author.substring(0,18) : news.author,
                      style: GoogleFonts.playfairDisplay(
                          fontSize: 15.0, fontWeight: FontWeight.bold),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(vertical: 13.0, horizontal: 8),
                child: Text(
                  news.title == null ? 'No Title' : news.title,
                  style: GoogleFonts.playfairDisplay(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Theme.of(context).primaryColor,
                  ),
                  // textAlign: TextAlign.,
                ),
              ),
              if (news.urlToImage != null)
                Image.network(
                  news.urlToImage,
                  width: MediaQuery.of(context).size.width * 1,
                ),
              Padding(
                padding: EdgeInsets.all(13.0),
                child: Text(
                  news.description == null
                      ? 'No Description'
                      : news.description,
                  style: GoogleFonts.playfairDisplay(
                      fontSize: 18, color: Color(0xFF393c40)),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 4,
                  softWrap: true,
                ),
              ),
              Container(
                child: Stack(
                  overflow: Overflow.visible,
                  children: [
                    Row(
                      children: [
                        Padding(
                          padding: EdgeInsets.only(left: 15),
                          child: IconButton(
                            icon: Icon(Icons.date_range),
                            onPressed: () {},
                          ),
                        ),
                        Text(
                          news.publishedAt == null
                              ? 'null'
                              : news.publishedAt.substring(0, 10),
                          style: TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.bold,
                              color: Theme.of(context).primaryColor),
                        ),
                      ],
                    ),
                    if (isAuth)
                      Positioned(
                        bottom: -20,
                        right: MediaQuery.of(context).size.width * .01,
                        child: Container(
                          height: 40,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            color: Theme.of(context).primaryColor,
                          ),
                          child: IconButton(
                            icon: Icon(Icons.bookmark),
                            color: Colors.white,
                            onPressed: () {
                              BlocProvider.of<NewsBloc>(context)
                                  .add(BookmarkNews(news: news));
                            },
                          ),
                        ),
                      ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
}
