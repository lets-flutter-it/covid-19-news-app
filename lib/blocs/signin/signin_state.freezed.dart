// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named

part of 'signin_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

class _$SignInStateTearOff {
  const _$SignInStateTearOff();

  Initital initial() {
    return const Initital();
  }

  Loading loading() {
    return const Loading();
  }

  SignInError signInError(String error) {
    return SignInError(
      error,
    );
  }
}

// ignore: unused_element
const $SignInState = _$SignInStateTearOff();

mixin _$SignInState {
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result signInError(String error),
  });
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result signInError(String error),
    @required Result orElse(),
  });
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(Initital value),
    @required Result loading(Loading value),
    @required Result signInError(SignInError value),
  });
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(Initital value),
    Result loading(Loading value),
    Result signInError(SignInError value),
    @required Result orElse(),
  });
}

abstract class $SignInStateCopyWith<$Res> {
  factory $SignInStateCopyWith(
          SignInState value, $Res Function(SignInState) then) =
      _$SignInStateCopyWithImpl<$Res>;
}

class _$SignInStateCopyWithImpl<$Res> implements $SignInStateCopyWith<$Res> {
  _$SignInStateCopyWithImpl(this._value, this._then);

  final SignInState _value;
  // ignore: unused_field
  final $Res Function(SignInState) _then;
}

abstract class $InititalCopyWith<$Res> {
  factory $InititalCopyWith(Initital value, $Res Function(Initital) then) =
      _$InititalCopyWithImpl<$Res>;
}

class _$InititalCopyWithImpl<$Res> extends _$SignInStateCopyWithImpl<$Res>
    implements $InititalCopyWith<$Res> {
  _$InititalCopyWithImpl(Initital _value, $Res Function(Initital) _then)
      : super(_value, (v) => _then(v as Initital));

  @override
  Initital get _value => super._value as Initital;
}

class _$Initital implements Initital {
  const _$Initital();

  @override
  String toString() {
    return 'SignInState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Initital);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result signInError(String error),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(signInError != null);
    return initial();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result signInError(String error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(Initital value),
    @required Result loading(Loading value),
    @required Result signInError(SignInError value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(signInError != null);
    return initial(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(Initital value),
    Result loading(Loading value),
    Result signInError(SignInError value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class Initital implements SignInState {
  const factory Initital() = _$Initital;
}

abstract class $LoadingCopyWith<$Res> {
  factory $LoadingCopyWith(Loading value, $Res Function(Loading) then) =
      _$LoadingCopyWithImpl<$Res>;
}

class _$LoadingCopyWithImpl<$Res> extends _$SignInStateCopyWithImpl<$Res>
    implements $LoadingCopyWith<$Res> {
  _$LoadingCopyWithImpl(Loading _value, $Res Function(Loading) _then)
      : super(_value, (v) => _then(v as Loading));

  @override
  Loading get _value => super._value as Loading;
}

class _$Loading implements Loading {
  const _$Loading();

  @override
  String toString() {
    return 'SignInState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other is Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result signInError(String error),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(signInError != null);
    return loading();
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result signInError(String error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(Initital value),
    @required Result loading(Loading value),
    @required Result signInError(SignInError value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(signInError != null);
    return loading(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(Initital value),
    Result loading(Loading value),
    Result signInError(SignInError value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading implements SignInState {
  const factory Loading() = _$Loading;
}

abstract class $SignInErrorCopyWith<$Res> {
  factory $SignInErrorCopyWith(
          SignInError value, $Res Function(SignInError) then) =
      _$SignInErrorCopyWithImpl<$Res>;
  $Res call({String error});
}

class _$SignInErrorCopyWithImpl<$Res> extends _$SignInStateCopyWithImpl<$Res>
    implements $SignInErrorCopyWith<$Res> {
  _$SignInErrorCopyWithImpl(
      SignInError _value, $Res Function(SignInError) _then)
      : super(_value, (v) => _then(v as SignInError));

  @override
  SignInError get _value => super._value as SignInError;

  @override
  $Res call({
    Object error = freezed,
  }) {
    return _then(SignInError(
      error == freezed ? _value.error : error as String,
    ));
  }
}

class _$SignInError implements SignInError {
  const _$SignInError(this.error) : assert(error != null);

  @override
  final String error;

  @override
  String toString() {
    return 'SignInState.signInError(error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other is SignInError &&
            (identical(other.error, error) ||
                const DeepCollectionEquality().equals(other.error, error)));
  }

  @override
  int get hashCode =>
      runtimeType.hashCode ^ const DeepCollectionEquality().hash(error);

  @override
  $SignInErrorCopyWith<SignInError> get copyWith =>
      _$SignInErrorCopyWithImpl<SignInError>(this, _$identity);

  @override
  @optionalTypeArgs
  Result when<Result extends Object>({
    @required Result initial(),
    @required Result loading(),
    @required Result signInError(String error),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(signInError != null);
    return signInError(error);
  }

  @override
  @optionalTypeArgs
  Result maybeWhen<Result extends Object>({
    Result initial(),
    Result loading(),
    Result signInError(String error),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signInError != null) {
      return signInError(error);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  Result map<Result extends Object>({
    @required Result initial(Initital value),
    @required Result loading(Loading value),
    @required Result signInError(SignInError value),
  }) {
    assert(initial != null);
    assert(loading != null);
    assert(signInError != null);
    return signInError(this);
  }

  @override
  @optionalTypeArgs
  Result maybeMap<Result extends Object>({
    Result initial(Initital value),
    Result loading(Loading value),
    Result signInError(SignInError value),
    @required Result orElse(),
  }) {
    assert(orElse != null);
    if (signInError != null) {
      return signInError(this);
    }
    return orElse();
  }
}

abstract class SignInError implements SignInState {
  const factory SignInError(String error) = _$SignInError;

  String get error;
  $SignInErrorCopyWith<SignInError> get copyWith;
}
