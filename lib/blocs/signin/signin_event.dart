import 'package:freezed_annotation/freezed_annotation.dart';

part 'signin_event.freezed.dart';

@freezed
abstract class SignInEvent with _$SignInEvent {
  const factory SignInEvent.signInWithEmail() = SignInWithEmail;
}
