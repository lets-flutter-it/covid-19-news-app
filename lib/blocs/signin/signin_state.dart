import 'package:freezed_annotation/freezed_annotation.dart';

part 'signin_state.freezed.dart';

@freezed
abstract class SignInState with _$SignInState {
  // Adjust initial spelling
  const factory SignInState.initial() = Initital;
  const factory SignInState.loading() = Loading;
  const factory SignInState.signInError(String error) = SignInError;
}
