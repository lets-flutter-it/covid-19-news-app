import 'package:bloc/bloc.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:news_app/blocs/auth/auth_bloc.dart';
import 'package:news_app/blocs/auth/auth_event.dart';
import 'package:news_app/models/user.dart';
import 'package:news_app/util/validator.dart';
import 'signin_event.dart';
import 'signin_state.dart';

class SignInBloc extends Bloc<SignInEvent, SignInState> {
  SignInBloc(this.authBloc);
  final AuthBloc authBloc;

  @override
  SignInState get initialState => SignInState.initial();

  final emailController = TextEditingController(),
      passwordController = TextEditingController();

  @override
  Stream<SignInState> mapEventToState(SignInEvent event) async* {
    final email = emailController.text, password = passwordController.text;
    if (!Validator.validateEmail(email)) {
      emailController.clear();
      yield SignInState.signInError('You must enter an email');
      yield SignInState.initial();
      return;
    }
    if (!Validator.validatePassword(password)) {
      passwordController.clear();
      yield SignInState.signInError('You must enter an email');
      yield SignInState.initial();
      return;
    }
    final error = SignInState.signInError('Invalid email or password');
    yield SignInState.loading();
    try {
      final result = await FirebaseAuth.instance
          .signInWithEmailAndPassword(email: email, password: password);
      final uid = result.user.uid;
      final doc = await Firestore.instance.document('users/$uid').get();
      if (!doc.exists) {
        await FirebaseAuth.instance.signOut();
        yield error;
      }
      authBloc.add(Authenticate(User.fromJson({
        'id': uid,
        ...doc.data,
      })));
    } catch (e) {
      print(e);
      yield error;
    }
  }
}
