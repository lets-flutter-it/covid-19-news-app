import 'package:flutter/foundation.dart';
import 'package:news_app/models/news.dart';

abstract class NewsEvent {}

class FetchNews extends NewsEvent {}

class BookmarkNews extends NewsEvent {
  BookmarkNews({@required this.news, this.shouldBookmark = true});
  final News news;
  final bool shouldBookmark;
}

class FetchBookmarks extends NewsEvent {}
