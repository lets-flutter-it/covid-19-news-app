import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:http/http.dart' as http;
import 'package:news_app/blocs/auth/auth_bloc.dart';
import 'package:news_app/blocs/auth/auth_state.dart';
import 'package:news_app/models/news.dart';

import 'news_event.dart';
import 'news_state.dart';

class NewsBloc extends Bloc<NewsEvent, NewsState> {
  NewsBloc(this.authBloc);

  final AuthBloc authBloc;

  @override
  NewsState get initialState => LoadingNews();

  static const api = 'http://newsapi.org/v2/everything?q=covid-19';
  static const apiKey = 'c0f7daa6ab424588b4d06495ea87859b';
  static const sortBy = 'publishedAt';

  final url = '$api&sortBy=$sortBy&pageSize=40&apiKey=$apiKey';

  @override
  Stream<NewsState> mapEventToState(NewsEvent event) async* {
    if (event is FetchNews) {
      try {
        final response = await http.get(url);
        final data = jsonDecode(response.body) as Map<String, Object>;

        final rest = (data['articles'] as List).cast<Map<String, Object>>();

        yield LoadedNews(
          feed: rest.map<News>((json) => News.fromJson(json)).toList(),
          bookmarked: [],
        );
      } catch (e) {
        print(e);
      }
    }

    if (event is BookmarkNews) {
      // if (authBloc.state is! Authenticated) return;

      final user = (authBloc.state as Authenticated).user;
      final news = event.news;

      final bookmark = News(
        title: news.title,
        description: news.description,
        author: news.author,
        content: news.content,
        publishedAt: news.publishedAt,
        url: news.url,
        urlToImage: news.urlToImage,
        isBookmarked: event.shouldBookmark,
      );

      final bookmarked = List<News>.from(state.bookmarked);

      if (event.shouldBookmark &&
          !bookmarked.any((n) => n.url == bookmark.url)) {
        yield LoadedNews(
          bookmarked: bookmarked..add(bookmark),
          feed: state.feed,
        );

        await Firestore.instance
            .document('users/${user.id}')
            .collection('bookmarks')
            .add(bookmark.toJson());
      } else if (!event.shouldBookmark &&
          bookmarked.any((n) => n.url == bookmark.url)) {
        yield LoadedNews(
          bookmarked: bookmarked..removeWhere((n) => n.url == bookmark.url),
          feed: state.feed,
        );

        final query = await Firestore.instance
            .document('users/${user.id}')
            .collection('bookmarks')
            .where('url', isEqualTo: bookmark.url)
            .getDocuments();

        for (final doc in query.documents) await doc.reference.delete();
      }
    }
    if (event is FetchBookmarks) {
      final user = (authBloc.state as Authenticated).user;
      final uid = user.id;
      final query = await Firestore.instance
          .collection('users')
          .document(uid)
          .collection('bookmarks')
          .orderBy('publishedAt', descending: true)
          .getDocuments()
          .then((value) => value.documents.map((e) {
                return e.data;
              }).toList());

      yield LoadedNews(
        feed: state.feed,
        bookmarked: query.map((json) => News.fromJson(json)).toList(),
      );
    }
  }
}
