import 'package:news_app/models/news.dart';

abstract class NewsState {
  const NewsState({this.feed = const [], this.bookmarked = const []});
  final List<News> feed, bookmarked;
}

class LoadingNews extends NewsState {}

class LoadedNews extends NewsState {
  const LoadedNews({List<News> feed, List<News> bookmarked, this.hasReachedMax})
      : super(
          feed: feed,
          bookmarked: bookmarked,
        );

  final bool hasReachedMax;

  LoadedNews copyWith({
    List<News> feed,
    List<News> bookmarked,
    bool hasReachedMax,
  }) {
    return LoadedNews(
      feed: feed ?? this.feed,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
      bookmarked: bookmarked ?? this.bookmarked,
    );
  }
}

class LoadingFail extends NewsState {}
